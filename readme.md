#### Generative Adversarial Networks (GANs) for Credit Card Fraud Data

The notebook in this repository accompanies the blog: https://www.toptal.com/machine-learning/generative-adversarial-networks

View this notebook on nbviewer: https://nbviewer.jupyter.org/github/codyznash/GANs_for_Credit_Card_Data/blob/master/GAN_comparisons.ipynb

This Jupyter notebook adapts GAN architectures from Waya.ai's GAN-Sandbox.

These GANs are applied to Kaggle's Credit Card Fraud Detection Dataset.


````
pip install git+https://github.com/pypa/pipenv.git@master

pipenv install
````
